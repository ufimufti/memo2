package com.bootcamp.memomvp.data.source

import com.example.memo2mvp.data.model.TaskEntity

interface TaskRepositoryInterface {

    fun insertTask(taskEntity: TaskEntity) : Long?

    fun getAllTask() : List<TaskEntity>?

    fun updateTask(taskEntity: TaskEntity) : Int?

    fun deleteTask(taskEntity: TaskEntity) : Int?
}