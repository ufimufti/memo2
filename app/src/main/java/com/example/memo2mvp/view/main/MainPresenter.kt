package com.example.memo2mvp.view.main

import com.example.memo2mvp.data.model.TaskEntity

interface MainPresenter {

    fun getAllTask()

    fun deleteTask(taskEntity: TaskEntity)


}
