package com.example.memo2mvp.view.update

import com.example.memo2mvp.data.model.TaskEntity

interface UpdatePresenter {
    fun updateTask(taskEntity: TaskEntity)
}
