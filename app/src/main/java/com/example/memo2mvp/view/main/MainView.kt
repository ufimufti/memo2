package com.example.memo2mvp.view.main

import com.example.memo2mvp.data.model.TaskEntity

interface MainView {
    fun getAllTask(list: List<TaskEntity>?)

    fun onDelete(result : String)

}
