package com.example.memo2mvp.view.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bootcamp.memomvp.data.source.TaskRepository
import com.bootcamp.memomvp.data.source.TaskRepositoryInterface
import com.example.memo2mvp.R
import com.example.memo2mvp.data.model.TaskEntity
import com.example.memo2mvp.view.task.TaskActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch



class SubMainActivity : AppCompatActivity(), MainView {
    lateinit var taskRepositoryInterface: TaskRepositoryInterface
    lateinit var MainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        taskRepositoryInterface = TaskRepository(this)
        MainPresenter = MainPresenterImplement(this, taskRepositoryInterface)


//        untuk rv
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        MainPresenter.getAllTask()
        swipeRefresh.setOnRefreshListener{
            MainPresenter.getAllTask()
            swipeRefresh.isRefreshing = false
        }
        val add : View = addFloating
        add.setOnClickListener { view ->
            val i = Intent(this, TaskActivity::class.java)
            startActivity(i)
        }
    }

    override fun getAllTask(list: List<TaskEntity>?) {
        GlobalScope.launch {
            runOnUiThread {
                list?.let{
                    val adapter = MainAdapter(it, MainPresenter)
                    recyclerView.adapter = adapter
                }
            }
        }
    }

    override fun onDelete(result: String) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
    }
}
