package com.example.memo2mvp.view.main

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.example.memo2mvp.R
import com.example.memo2mvp.data.model.TaskEntity
import com.example.memo2mvp.view.update.UpdateActivity
import kotlinx.android.synthetic.main.list_view_task.view.*

class MainAdapter(val listTask : List<TaskEntity>, val MainPresenter: MainPresenter) : RecyclerView.Adapter<MainAdapter.ListTaskViewHolder>() {
    inner class ListTaskViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.tvTitleList
        val description = view.tvDescriptionList

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTaskViewHolder {
        return ListTaskViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_view_task, parent, false))
    }

    override fun onBindViewHolder(holder: ListTaskViewHolder, position: Int) {
        listTask.get(position).let {
            holder.title.text = it.title
            holder.description.text = it.description
        }

        holder.itemView.btnEdit.setOnClickListener {
            val intentEditActivity = Intent(it.context, UpdateActivity::class.java)

            intentEditActivity.putExtra("Task", listTask[position])
            it.context.startActivity((intentEditActivity))
        }

        holder.itemView.btnDelete.setOnClickListener{
            AlertDialog.Builder(it.context).setPositiveButton("Ya"){ p0, p1 ->
                MainPresenter.deleteTask(listTask.get(position))
            }.setNegativeButton("Tidak"){ p0, p1 ->
                p0.dismiss()
            }.setMessage("Apa kamu yakin menghapus data ${listTask[position].id}").setTitle("Konfirmasi Hapus").create().show()


    }

}
    override fun getItemCount(): Int {
        return listTask.size
    }

}
