package com.example.memo2mvp.view.main

import com.bootcamp.memomvp.data.source.TaskRepositoryInterface
import com.example.memo2mvp.data.model.TaskEntity

class MainPresenterImplement (private val MainView: MainView, private val taskRepositoryInterface: TaskRepositoryInterface) : MainPresenter {
    override fun getAllTask() {
        val result = taskRepositoryInterface.getAllTask()

        MainView.getAllTask(result)
    }

    override fun deleteTask(taskEntity: TaskEntity) {
        val result = taskRepositoryInterface.deleteTask(taskEntity)

        if (result != 0){
            MainView.onDelete("Delete Success")
        } else {
            MainView.onDelete("Delete Failed")
        }
    }

}
