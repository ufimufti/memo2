package com.example.memo2mvp.view.task

import com.bootcamp.memomvp.data.source.TaskRepositoryInterface
import com.example.memo2mvp.data.model.TaskEntity

class TaskPresenterImplement(private val taskView: TaskView,
                             private val taskRepositoryInterface: TaskRepositoryInterface) : TaskPresenter
    {
        override fun insertTask(taskEntity: TaskEntity) {
            val result  = taskRepositoryInterface.insertTask(taskEntity)

            if (result != 0.toLong()){
                taskView.onSuccessInsert("Insert Data Success")
            }else{
                taskView.onSuccessInsert("Insert Data Failed")
            }
        }

    }