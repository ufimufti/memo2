package com.example.memo2mvp.view.update

import com.bootcamp.memomvp.data.source.TaskRepository
import com.bootcamp.memomvp.data.source.TaskRepositoryInterface
import com.example.memo2mvp.data.model.TaskEntity

class UpdatePresenterImplement(private val updateView: UpdateView,
                               private val taskRepositoryInterface: TaskRepositoryInterface
) : UpdatePresenter {
    override fun updateTask(taskEntity: TaskEntity) {
        val result = taskRepositoryInterface.updateTask(taskEntity)

        if (result != 0){
            updateView.onSuccessUpdateDong("Success Updated")
        } else {
            updateView.onSuccessUpdateDong("Failed Updated")
        }
    }
}