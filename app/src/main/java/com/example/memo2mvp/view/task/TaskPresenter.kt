package com.example.memo2mvp.view.task

import com.example.memo2mvp.data.model.TaskEntity

interface TaskPresenter {
    fun insertTask(taskEntity: TaskEntity)
}
