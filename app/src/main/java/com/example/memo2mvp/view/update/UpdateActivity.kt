package com.example.memo2mvp.view.update

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bootcamp.memomvp.data.source.TaskRepository
import com.bootcamp.memomvp.data.source.TaskRepositoryInterface
import com.example.memo2mvp.MainActivity
import com.example.memo2mvp.R
import com.example.memo2mvp.data.model.TaskEntity
import kotlinx.android.synthetic.main.activity_edit.*

class UpdateActivity: AppCompatActivity(), UpdateView {

lateinit var taskRepositoryInterface: TaskRepositoryInterface
lateinit var updatePresenter: UpdatePresenter

    val data: TaskEntity? by lazy {
        intent.getParcelableExtra<TaskEntity>("data")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        taskRepositoryInterface = TaskRepository(this)
        updatePresenter = UpdatePresenterImplement(this, taskRepositoryInterface)

        etTaskTitleEdit.setText(data?.title)
        etDescriptionEdit.setText(data?.description)

        setSupportActionBar(tbEdit)

        fbListTask.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onSuccessUpdateDong(result: String) {
        etDescriptionEdit.setText("")
        etTaskTitleEdit.setText("")
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.menuSave -> {
                val title = etTaskTitleEdit.text.toString()
                val description = etDescriptionEdit.text.toString()
                updatePresenter.updateTask(TaskEntity(data?.id,title,description))
                true
            }else -> return super.onOptionsItemSelected(item)
        }
    }


}